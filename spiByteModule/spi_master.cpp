#include <Arduino.h>
#include <SPI.h>

#define LED 13
#define nCS 10

uint8_t temp;
uint8_t miso;
uint8_t i;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(nCS, OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  SPI.begin();
  Serial.println("SPI byte mode");
}

void loop() {
  for(i = 0; i < 256; ++i){
    digitalWrite(LED, 1);
    digitalWrite( nCS, 0 );
    SPI.beginTransaction( SPISettings( 1000000, MSBFIRST, SPI_MODE3 ) );
    miso = SPI.transfer(i);
    SPI.endTransaction();
    digitalWrite( nCS, 1 );
    digitalWrite(LED, 0);

    Serial.print("Sent Byte: ");
    Serial.print(i, HEX);
    Serial.print(", Received Byte: ");
    Serial.print(miso, HEX);
    Serial.print("\r\n");

    delay(1000);
  }
}

