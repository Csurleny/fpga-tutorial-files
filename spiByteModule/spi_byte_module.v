`timescale 1ns/1ps
/* SPI communication module based on the implementation at http://fpga4fun.com/SPI2.html
 * Tested on platforms: Altera Cyclone VI, and Lattice MachXO2
 * 
 * Description: SPI slave module for communication with an external microcontroller.
 * The SPI side has the | sclk | mosi | miso | cs_n | pins
 * The parallel side has the 8b wide TX and RX data buses, 
 * a system clock line, and an rx_valid line.
 */
 
module spi_byte_module( input wire clk,      // internal FPGA clock
                    input wire sclk,        // SPI clock
						  input wire mosi,        // SPI master out, slave in
						  output wire miso,       // SPI slave in, master out
						  input wire cs_n,          // SPI slave select
						  input wire [7:0] tx,    // BYTE to transmit
						  output wire [7:0] rx,   // BYTE received
						  output wire rx_valid );  // BYTE received is valid

	// Synchronize sclk to FPGA domain clock using a two-stage shift-register,
	//   where bit [0] takes the hit of timing errors.
	// For sclk and cs_n a third stage is used to detect rising/falling
	reg [2:0] SCLK_r;  always @(posedge clk) SCLK_r <= { SCLK_r[1:0], sclk };
	reg [2:0] SS_r;    always @(posedge clk) SS_r   <= {   SS_r[1:0],   cs_n };
	reg [1:0] MOSI_r;  always @(posedge clk) MOSI_r <= {   MOSI_r[0], mosi };
	wire SCLK_rising  = ( SCLK_r[2:1] == 2'b01 );
	wire SCLK_falling = ( SCLK_r[2:1] == 2'b10 );
	wire SS_falling   = ( SS_r[2:1] == 2'b10 );
	wire SS_active    = ~SS_r[1];   // synchronous version of ~cs_n input
	wire MOSI_sync    = MOSI_r[1];  // synchronous version of mosi input

	// circular buffer, initialized with data to be transmitted	
	// - on SCLK_falling, bit [7] is transmitted by through MISO_r
	// - on SCLK_rising, MOSI_sync is shifted in as bit [0]
	// see http://www.coertvonk.com/technology/logic/connecting-fpga-and-arduino-using-spi-13067/3#operation

	reg [7:0] buffer = 8'hxx;

	// current state logic

	reg [2:0] state = 3'bxxx; // state corresponds to bit count
	
	always @(posedge clk)
		if ( SS_active )
			begin
				if ( SS_falling )   // start of 1st byte
					state <= 3'd0;
				if ( SCLK_rising )  // input bit available
					state <= state + 3'd1;
			end

	// input/output logic
	
	assign rx      = {buffer[6:0], MOSI_sync};       // bits received so far
	assign rx_valid = (state == 3'd7) && SCLK_rising; // BYTE received is valid

	reg MISO_r = 1'bx;	
	assign miso = SS_active ? MISO_r : 1'bz;
	
	always @(posedge clk)
		if( SS_active )
			begin
			
				if( SCLK_rising )         // INPUT on rising SPI clock edge
					if( state != 3'd7 ) 
						buffer <= rx;
								
				if( SCLK_falling)         // OUTPUT on falling SPI clock edge
					if ( state == 3'b000 )
						begin 
							MISO_r <= tx[7];    //   start by sending the MSb
							buffer <= tx;       //   remaining bits are send from buffer
						end
					else
						MISO_r <= buffer[7];  //   send next bit

			end
						
endmodule
