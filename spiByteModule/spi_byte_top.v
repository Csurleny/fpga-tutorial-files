`timescale 1ns/1ps


module spi_byte_top(
	input sclk,
	input mosi,
	input cs_n,
	output miso,
	output reg [7:0] LED,
	output clk_locked
	
);

	wire clk_wire_7MHz;
	wire clk_wire_105MHz;
	
// Internal clock instance
	OSCH OSCH_inst( 
		.STDBY(1'b0), 		
		.OSC(clk_wire_7MHz),
		.SEDSTDBY());	
// Clock setting for the internal oscillator
	defparam OSCH_inst.NOM_FREQ = "7.00";
	
	
	ext_osc_pll pll_inst (
		.CLKI(clk_wire_7MHz), 
		.CLKOP(clk_wire_105MHz),
		.LOCK(clk_locked)
	);
	
	wire [7:0] rx_wire;
	reg [7:0] tx_reg = 8'd0;
	wire rx_valid_wire;
	
// The SPI module instance
	spi_byte_module spi_byte_module_instance (
		.clk(clk_wire_105MHz),
		.sclk(sclk),
		.mosi(mosi),
		.miso(miso),
		.cs_n(cs_n),
		.tx(tx_reg),
		.rx(rx_wire),
		.rx_valid(rx_valid_wire)
	);
	
	// Internal data loopback - the current TX data 
	// is the previous RX data
	always @(posedge clk_wire_105MHz)
		if(rx_valid_wire)
			tx_reg <= rx_wire;
	
// Display data when 8b is received
	always @(posedge clk_wire_105MHz)
		if(rx_valid_wire)
			LED <= rx_wire; 
			
	
	
endmodule
