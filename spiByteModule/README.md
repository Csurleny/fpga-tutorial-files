# SPI_byte_module
Lowest level design of the hardware accelerator  
spi_byte_module.v - contains the SPI state machine and data path for the basys of the serial to 8b parallel communication interface.  
spi_byte_test.v - contains the testbench of the spi_byte_module.v  
spi_byte_top.v - contains the top module including the internal oscillator, pll, spi_byte instance, and output register.  
spi_interface.lpf - the i/o constraint file for the MachXO2 FPGA.  
spi_master.cpp - contains the SPI master code for the Arduino.  
