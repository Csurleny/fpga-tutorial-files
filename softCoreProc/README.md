# SoftCoreProc
Project files for the LM8 tutorial  
Contents:  
  
lm8_top.v - the top module of the FPGA implementation. It contains the pin definitions, the internal oscillator, BCD to 7 Segment decoder, and the LM8 soft microcontroller.  
LM8_UART_Test.c - is the modified UART test file, which contains the program which will be compiled for the microcontroller.  
micro8test.lpf - the timing and location constraints for our pin definitions.  
