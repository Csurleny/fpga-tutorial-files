`include "../soc/lm8Test.v"
`include "../../mcu_pll.v"
`include "../../../Digits/SegmentLogic_verilog.v"

module platform1_top(reset_n, uartSIN, uartSOUT, Segment_out);
	
	input reset_n, uartSIN;
	output uartSOUT;
	output[6:0] Segment_out;
	
	wire clk_w;
	
	OSCH OSCH_inst (.OSC(clk_w), .SEDSTDBY(), .STDBY(1'b0));
	defparam OSCH_inst.NOM_FREQ = "24.18";
	
//	mcu_pll pll_inst(
//		.CLKI(slow_clk_w),
//		.CLKOP(fast_clk_w)
//		);
		
	wire[3:0] d_bcd_w;	
	
	SegmentLogic sl_inst(
		.din(d_bcd_w), 
		.dout(Segment_out)
		);

	lm8Test lm8Test_u (
		.clk_i (clk_w),
		.reset_n (reset_n),
		.LEDPIO_OUT (d_bcd_w),
		.uartSIN (uartSIN),
		.uartSOUT (uartSOUT)
	);
	
	
	
endmodule
