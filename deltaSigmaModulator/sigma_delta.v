module sigma_delta#(
    parameter 
    ADC_BIT = 8,
    ACCUM_BIT = 10,
    LPF_DEPTH_BIT = 3)(
    input i_clk,
    input i_rstn,
    input i_cmp,
    output o_cmp,
    output o_rdy,
    output [ADC_BIT-1:0] o_data);

    reg r_delta;
    reg [ACCUM_BIT-1:0] r_sigma;
    reg [ADC_BIT-1:0] r_accum;
    reg [ACCUM_BIT-1:0] r_counter;
    reg r_rollover;
    reg r_accum_rdy;

    always @(posedge i_clk)begin
        r_delta <= i_cmp;
	end
    
    assign o_cmp = r_delta;

    always @(posedge i_clk) begin
        if(~i_rstn) begin
            r_sigma <= 0;
            r_accum <= 0;
            r_accum_rdy <= 0;
        end else begin
            if(r_rollover) begin
                r_accum <= r_sigma[ACCUM_BIT-1:ACCUM_BIT-ADC_BIT];
                r_sigma <= r_delta;
            end else 
                if(&r_sigma != 1'b1)
                    r_sigma <= r_sigma + r_delta;
            r_accum_rdy <= r_rollover;
        end
    end

    avg #(.ACD_BIT(ADC_BIT), .LPF_DEPTH_BIT(LPF_DEPTH_BIT))
     avg_instance (.i_clk(i_clk), .i_rstn(i_rstn), .i_sample(r_accum_rdy), 
     .i_data(r_accum), .o_data(o_data), .o_data_valid(o_rdy));

     always @(posedge i_clk) begin
        if(~i_rstn) begin
            r_counter <= 0;
            r_rollover <= 0;
        end else begin
            r_counter <= r_counter + 1;
            r_rollover <= &r_counter;
        end
     end

endmodule
