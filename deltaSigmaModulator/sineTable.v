module sineTable(
    input i_clk,
    input i_rstn,
    output reg [7:0]o_sin
);

reg[7:0] r_sin [0:15];
integer i, k;

initial begin
    i = 0;
    k = 0;
    r_sin[0]=131;
    r_sin[1]=144;
    r_sin[2]=157;
    r_sin[3]=170;
    r_sin[4]=182;
    r_sin[5]=194;
    r_sin[6]=205;
    r_sin[7]=215;
    r_sin[8]=224;
    r_sin[9]=232;
    r_sin[10]=239;
    r_sin[11]=245;
    r_sin[12]=249;
    r_sin[13]=252;
    r_sin[14]=254;
    r_sin[15]=254;
end

always @(posedge i_clk) begin
    if(i_rstn == 0)begin
        i = 0;
        o_sin <= 0;
    end
    else begin
        if(k == 0)
            o_sin <= r_sin[i%16];
        else if(k == 1)
            o_sin <= r_sin[15 - i % 16];
        else if(k == 2)
            o_sin <= (254 - r_sin[i%16]);
        else
            o_sin <= (254 - r_sin[15 - i % 16]);
        if (i % 16 == 15)
            k = (k + 1) % 4;

        if(i == 63)
            i = 0;
        else
            i = i + 1;
    end
    
end



endmodule
