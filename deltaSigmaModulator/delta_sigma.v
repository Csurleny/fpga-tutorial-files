module delta_sigma#(
    parameter DATA_WIDTH=8)(
    input i_clk, i_rstn, 
    input[DATA_WIDTH-1:0] i_data,
    output reg o_data);

    reg [DATA_WIDTH+1:0] r_delta, r_sigma, r_sigma_latched, r_delta_b;

    always @(posedge i_clk) begin
        if(~i_rstn) begin
            r_sigma_latched <= 0;
            o_data <= 0;
        end
        else begin
            r_sigma_latched <= r_sigma;
            o_data <= r_sigma_latched[DATA_WIDTH+1];
        end
    end
	
	always @(*) begin
		r_delta_b = {2{r_sigma_latched[DATA_WIDTH+1]}} << DATA_WIDTH;
		r_delta = i_data + r_delta_b;
		r_sigma = r_delta + r_sigma_latched;

	end

endmodule