# Delta-Sigma Modulator, DAC and ADC
Project files for the DSM Tutorial  
Contents:  

LUT_generator.py - generates the quarter wave sine function lookup table for the DAC
modulator_test.py - comparison of a MOD1 and MOD2 DSM algorithm
avg.v - parametric moving average filter with included decimation
delta_sigma.v - delta sigma modulator module
pwm.v - pulse width modulator module
sigma_delta.v - sigma delta modulator module
sineTable.v - sine wave generator based on LUTs
topModule.v - the top module of the design, containing the two DACs and the ADC
delta_sigma.lpf - the pin configuration of the design
