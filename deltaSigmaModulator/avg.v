module avg#(
    parameter 
    ACD_BIT = 8, 
    LPF_DEPTH_BIT = 4)(
    input i_clk,
    input i_rstn,
    input i_sample,
    input [ACD_BIT-1:0] i_data,
    output reg [ACD_BIT-1:0] o_data,
    output o_data_valid
);

reg [ACD_BIT+LPF_DEPTH_BIT-1:0]      r_accum;          // accumulator
reg [LPF_DEPTH_BIT-1:0]              r_count;          // decimation count
reg [ACD_BIT-1:0]  					 r_raw_data_d1;    // pipeline register

reg r_sample_d1, r_sample_d2;                               // pipeline registers
reg r_result_valid;                                       // accumulator result 'valid'
wire w_accumulate;                                        // sample rising edge detected
wire w_latch_result;                                      // latch accumulator result

always @(posedge i_clk) begin
    if(~i_rstn) begin
        r_sample_d1 <= 0;
        r_sample_d2 <= 0;
        r_raw_data_d1 <= 0;
        r_result_valid <= 0;
    end else begin
        r_sample_d1 <= i_sample;
        r_sample_d2 <= r_sample_d1;
        r_raw_data_d1 <= i_data;
        r_result_valid <= w_latch_result;
    end
end

assign w_accumulate = r_sample_d1 && !r_sample_d2;
assign w_latch_result = w_accumulate && (r_count == 0);

always @(posedge i_clk) begin
    if(~i_rstn)
        r_count <= 0;
    else
        if(w_accumulate) 
            r_count <= r_count + 1;
end

always @(posedge i_clk) begin
    if(~i_rstn)
        r_accum <= 0;
    else
        if(w_accumulate)
            if(r_count == 0)
                r_accum <= r_raw_data_d1;
            else
                r_accum <= r_accum + r_raw_data_d1;
end

always @(posedge i_clk) begin
    if(~i_rstn)
        o_data <= 0;
    else if(w_latch_result)
        o_data <= r_accum >> LPF_DEPTH_BIT;
end

assign o_data_valid = r_result_valid;


endmodule
