import numpy as np
from matplotlib import pyplot as plt


if __name__ == '__main__':
    N = 16
    M = 8
    k = np.linspace(0, np.pi/2, N)
    x = ((np.sin(k + 0.03) + 1.0)/2.0) * (np.power(2, M)-1)
    x = np.array(x, dtype=int)


    with open('sin_table.txt', 'w') as f:
        for ii in range(N):
            f.write(f"r_sin[{ii}]={x[ii]};\r")

    k = 0
    y = []
    for ii in range(8*N):
        if k == 0:
            y.append(x[ii % N])
        elif k == 1:
            y.append(x[N-1 - ii % N])
        elif k == 2:
            y.append(-x[ii % N] + max(x))
        else:
            y.append(-x[N-1 - ii % N] + max(x))

        if ii % N == N-1:
            k = (k + 1) % 4

    plt.figure()
    plt.plot(y)
    plt.show()

    t = np.arange(0, 2*np.pi, np.pi/100)
    x = np.sin(t)
    plt.figure()
    plt.plot(t, x)
    i, = np.where(np.isclose(t, np.pi/2))
    plt.plot([0, 0], [-1, 1], [t[i], t[i]], [-1, 1], [2*t[i], 2*t[i]], [-1, 1],
             [3*t[i], 3*t[i]], [-1, 1], [4*t[i], 4*t[i]], [-1, 1], color='black')
    plt.grid(True)
    plt.ylim(-1, 1)
    plt.xlim(0, 2*np.pi)
    plt.show()


