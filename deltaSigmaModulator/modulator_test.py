import numpy as np
from matplotlib import pyplot as plt


if __name__ == '__main__':
    # input signal generator
    t = np.arange(0, 5, 0.01)
    u = (np.sin(2*np.pi*t) + 1.0)/2.0

    # MOD1 delta-sigma algorithm
    v1 = np.zeros(t.shape)
    x2 = np.zeros((2, 1))
    for ii, element in enumerate(u):
        if ii == 0:
            x1 = element
            x2[0] = x1
        else:
            x1 = element - v1[ii-1]
            x2[0] = x1 + x2[1]
        v1[ii] = (np.sign(x2[0]) + 1.0)/2.0
        x2[1] = x2[0]

    # MOD2 delta-sigma algorithm
    v2 = np.zeros(t.shape)
    x2 = np.zeros((2, 1))
    x3 = np.zeros((2, 1))
    for ii, element in enumerate(u):
        if ii == 0:
            v = 0
        else:
            v = v2[ii-1]
        x2[0] = x2[1] + element - v
        x3[0] = x3[1] + x2[0] - v
        v2[ii] = (np.sign(x3[0])+1.0)/2.0
        x2[1] = x2[0]
        x3[1] = x3[0]

    plt.figure()
    plt.subplot(211)
    plt.plot(t, u)
    plt.plot(t, v1)
    plt.subplot(212)
    plt.plot(t, u)
    plt.plot(t, v2)
    plt.show()

