module topModule(
	input i_adc_p,
	output o_adc_n,
    output pwm_out,
	output ds_out,
	output led);
	
	wire osc_clk;
	
	defparam OSCH_inst.NOM_FREQ = "53.20";
	OSCH OSCH_inst( 
		.STDBY(1'b0), 
		.OSC(osc_clk),
		.SEDSTDBY()
		); 
		
	wire[7:0] duty_w;
	
	delta_sigma ds_instance(
		.i_clk(osc_clk),
		.i_rstn(1'b1), 
		.i_data(duty_w),
		.o_data(ds_out)
	);

	
	reg[18:0] divider_reg;
	reg slow_clk_r;
	
	initial begin
		slow_clk_r = 0;
	end
	
	always @(posedge osc_clk) begin
		if(divider_reg == 19'd265999)begin
			divider_reg = 19'd0;
			slow_clk_r = ~slow_clk_r;
		end
		else
			divider_reg = divider_reg + 1;
	end

    sineTable sineTable_instance (
        .i_clk(slow_clk_r),
        .i_rstn(1'b1),
        .o_sin(duty_w)
    );
	
	assign led = slow_clk_r;
	
	wire w_adc_rdy;
	wire [7:0] w_adc_digital;
	reg [7:0] r_adc_digital;
	
	sigma_delta adc_instance(
		.i_clk(osc_clk),
		.i_rstn(1'b1),
		.i_cmp(i_adc_p),
		.o_cmp(o_adc_n),
		.o_rdy(w_adc_rdy),
		.o_data(w_adc_digital)
	);
	
	always @(posedge osc_clk) begin
		if(w_adc_rdy)
			r_adc_digital <= w_adc_digital;
	end
	
	pwm pwm_instance(
        .clk(osc_clk),
        .rst_n(1'b1), 
        .duty(r_adc_digital), 
        .pwm_out(pwm_out)
    );
			
			
	
endmodule