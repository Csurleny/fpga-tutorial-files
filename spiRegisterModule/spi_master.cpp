#include <Arduino.h>
#include <SPI.h>

// FPGA Chip Select Pin number
const uint8_t CS = 10; 
int jj;

// Commands:
// 0x00 - 0x0F operation command zone (16 operations can be used in this configuration)
// 0x80 - 0x8F read the content of a register (16 x 32b registers)
// 0xC0 - 0xCF write to the registers (16 x 32b registers)


enum commands_t{
    CMD_STATUS  = 0x00,
    CMD_START   = 0x01,
    CMD_STOP    = 0x02,
    CMD_OP1     = 0x03,
    CMD_OP2     = 0x04,
    CMD_CLRINT  = 0x05,
    CMD_RDREG   = 0x80,
    CMD_WRREG   = 0xC0
};

uint8_t DUMMY = 0xFF;

uint8_t exchange_byte( uint8_t const value )
{
    return SPI.transfer( value );
}

uint8_t read_status( void )
{
    (void)exchange_byte( CMD_STATUS );
    return exchange_byte( DUMMY );
}

void write_register( uint8_t const regNr, uint32_t const value )
{
    (void)exchange_byte( CMD_WRREG | (regNr & 0xFF) );
    (void)exchange_byte( (value >> 24) & 0xFF );
    (void)exchange_byte( (value >> 16) & 0xFF );
    (void)exchange_byte( (value >>  8) & 0xFF );
    (void)exchange_byte( (value >>  0) & 0xFF );
}

uint32_t read_register( uint8_t const regNr )
{
    (void)exchange_byte( CMD_RDREG | (regNr & 0xFF) );
    uint32_t const b1 = exchange_byte( DUMMY );
    uint32_t const b2 = exchange_byte( DUMMY );
    uint32_t const b3 = exchange_byte( DUMMY );
    uint32_t const b4 = exchange_byte( DUMMY );
    return (b1 << 24) | (b2 << 16) | (b3 << 8) | (b4 << 0);
}

void print_status_error( uint8_t const val1,
                         uint8_t const val2 )
{
    Serial.print( "STATUS error: " );
    Serial.print( val1, HEX ); Serial.print( " != " ); Serial.println( val2, HEX );
}

void print_reg_error( char const * const str,
                      uint8_t const regNr,
                      uint32_t const val1,
                      uint32_t const val2 )
{
    Serial.print( str ); Serial.print( "(" );
    Serial.print( regNr, HEX ); Serial.print( ") error: " );
    Serial.print( val1, HEX ); Serial.print( " != " ); Serial.println( val2, HEX );
}

void read_verify_status( uint8_t const expected )
{
    uint8_t const status = read_status();
    if ( status != expected ) {
        print_status_error( status, expected );
    }else
    {
        Serial.println("Correct Status");
    }
    
}

void write_verify_register( uint8_t const regNr, uint32_t const value )
{
    write_register( regNr, value );
    uint32_t const valRead = read_register( regNr );
    if ( valRead != value ) {
        print_reg_error( "WRREG ", regNr, valRead, value );
    }
}

uint32_t read_verify_register( uint8_t const regNr, uint32_t const expected )
{
    uint32_t const valRead = read_register( regNr );
    if ( valRead != expected ) {
        print_reg_error( "RDREG ", regNr, valRead, expected );
    }
    return valRead;
}

void setup()
{
    Serial.begin( 115200 );
	while (!Serial) {
		; // wait
	}
	Serial.println("spi_msg");
	SPI.begin();
    pinMode( CS, OUTPUT );
}

int response = 0;
uint32_t reg_data = 0;
uint8_t reg_nr = 0;

void loop()
{
    Serial.println("Choose a test routine:");
    Serial.println("1 - Status check.");
    Serial.println("2 - Write test.");
    Serial.println("3 - Read test.");
    Serial.println("4 - Flag test.");

    while(Serial.available() <= 0);
    response = Serial.read();

    switch (response)
    {
    case 49:
        digitalWrite(CS, 0);
	    SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
        read_verify_status(0x69);
        SPI.endTransaction();
	    digitalWrite(CS, 1);
        break;

    case 50:
        Serial.println("Write test: Add the register number (0 or 1):");
            while(Serial.available() <= 0);
            response = Serial.read();

            if(response == 48){
                reg_data = 0x76543210;
                reg_nr = 0;
            }

            if(response == 49){
                reg_data = 0x01234567;
                reg_nr = 1;
            }

            digitalWrite(CS, 0);
            SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
            write_verify_register(reg_nr, reg_data);

            SPI.endTransaction();
            digitalWrite(CS, 1);
        break;
    case 51:
        Serial.println("Read test: Add the register number (0 to 3):");
        while(Serial.available() <= 0);
        response = Serial.read();
        digitalWrite(CS, 0);
        SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
        read_verify_register(response - 48, 0xDEADBEEF);
        SPI.endTransaction();
	    digitalWrite(CS, 1);
        break;

    case 52:
        digitalWrite(CS, 0);
        SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
        (void)exchange_byte( CMD_START );
        SPI.endTransaction();
	    digitalWrite(CS, 1);
        break;
    
    default:
        Serial.println("Error: wrong choice, try again...");
        break;
    }

}