`timescale 1ns/1ps


module spi_msg_top#(
	parameter NR_RWREGS = 2,
	parameter NR_ROREGS = 2)(
	input sclk,
	input mosi,
	input cs_n,
	output miso,
	output reg [7:0] LED,
	output clk_locked,
	
);

	wire clk_wire_7MHz;
	wire clk_wire_105MHz;
	
// Internal clock instance
	OSCH OSCH_inst( 
		.STDBY(1'b0), 		
		.OSC(clk_wire_7MHz),
		.SEDSTDBY());	
// Clock setting for the internal oscillator
	defparam OSCH_inst.NOM_FREQ = "7.00";
	
	
	ext_osc_pll pll_inst (
		.CLKI(clk_wire_7MHz), 
		.CLKOP(clk_wire_105MHz),
		.LOCK(clk_locked)
	);
	
	wire rx_valid;
	wire [7:0] rx, tx;
	wire [NR_RWREGS+NR_ROREGS-1:0] [31:0] registers; // registers are defined in spi_msg_if, 'cause output of that module can only connect to a net, not a register
	wire [NR_RWREGS-1:0] wrRegs;
	
// The SPI module instance
	spi_byte_module spi_byte_module_instance (
		.clk(clk_wire_105MHz),
		.sclk(sclk),
		.mosi(mosi),
		.miso(miso),
		.cs_n(cs_n),
		.tx(tx),
		.rx(rx),
		.rx_valid(rx_valid)
	);
	
	wire flag_wire;
	
	spi_msg_module #(
		.NR_RWREGS(NR_RWREGS),
		.NR_ROREGS(NR_ROREGS)
	) spi_msg_module_instance (
		.clk(clk_wire_105MHz),
		.tx(tx),
		.rx(rx),
		.rx_valid(rx_valid),
		.rwRegs(registers[NR_RWREGS -1:0]),
		.roRegs(registers[NR_RWREGS + NR_ROREGS -1:NR_RWREGS]),
		.wrRegs(wrRegs),
		.flag_out(flag_wire),
	);
	
	// connect remaining read-only registers
	assign registers[NR_RWREGS+0] = 32'hDEADBEEF;
	genvar nn;
	generate
		for (nn = 1; nn < NR_ROREGS; nn = nn + 1 )
			begin :nnRegs
				assign registers[NR_RWREGS+nn] = 32'h00000000;
			end
   endgenerate
	
	// turn LEDs on when expected value has been written to register
	assign LED[0] = (registers[0] == 32'h76543210 );
	assign LED[1] = (registers[1] == 32'h01234567 );
	assign LED[6:2] = 6'd0;
	assign LED[7] = flag_wire;
	
			
	
	
endmodule
