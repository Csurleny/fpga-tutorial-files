`timescale 1ns/1ps

module spi_msg_tb;
	localparam	t_spi = 250;    // 4MHz spi clk
	localparam	t_sys = 5;		// 200MHz system clock input
	
	reg clk_200MHz = 1'b0;
	reg sclk = 1'b1;
	wire miso;
	reg mosi = 1'bz;
	reg cs_n = 1'b1;
	reg status = 1'b0;
	reg [7:0] LEDS;
	
	wire [1:0] LED;
	reg [7:0] dummy = 8'hxx;
	wire clkLocked;
	
	spi_byte_module spi_byte_module_instance (
		.clk(clk_200MHz),
		.sclk(sclk),
		.mosi(mosi),
		.miso(miso),
		.cs_n(cs_n),
		.tx(8'h55),
		.rx(rx_wire),
		.rx_valid(rx_valid_wire)
	);
	
	always @(posedge clk_200MHz)
		if(rx_valid_wire)
			LEDS <= rx_wire;
	
	
	
	always forever
		#(t_sys/2) clk_200MHz = ~clk_200MHz;
		
		reg [7:0] temp_reg = 8'h00;
		integer j;
			
	initial begin
		#100
		for(j = 0; j < 256 ; j = j + 1) begin 
			cs_n = 1'b0;
			exchange_byte(temp_reg, misoData);
			#10
			cs_n = 1'b1;
			#10
			temp_reg = temp_reg + 8'h01;
		end
	
		$display("EOT");
		$stop;
	end
	
	task exchange_byte(input [7:0] mosiData, output [7:0] misoData);
		integer i;
		begin
			for( i = 0; i < 8; i = i + 1)begin
				#(t_spi/2) 
				sclk = 1'b0;
				mosi = mosiData[7-i];
				#(t_spi/2)
				sclk = 1'b1;
				misoData = {misoData[6:0], miso};
			end
		end	
	endtask
	
endmodule
