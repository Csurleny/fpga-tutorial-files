# SPI_register_mode

spi_byte_module.v - the 8b RX and TX module from the previous tutorial.  
spi_msg_module.sv - the register module with the state machine and storage/control registers.  
spi_msg_top.sv    - the top module with internal oscillator, pll, byte and reg modules, and the output logic.  
spi_msg_test.v    - the verilog testbench module.  
spi_interface.lpf - the i/o constraints for the MachXO2 FPGA.  
spi_master.cpp    - arduino code with simple terminal menu and spi master imlementation for FPGA test.  
